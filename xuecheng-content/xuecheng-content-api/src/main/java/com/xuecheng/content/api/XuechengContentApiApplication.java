package com.xuecheng.content.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuechengContentApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuechengContentApiApplication.class, args);
    }

}
